//Асинхронность позволяет, не дожидаясь результата выполнения текущих задач, продолжать выполнение кода

document.querySelector(".js-btn").addEventListener("click", () => {
  getData();
});

async function getData() {
  const response = await fetch("https://api.ipify.org/?format=json");
  const userIp = await response.json();
  await document.querySelector(".js-div").remove();
  const userAdress = await fetch(
    `http://ip-api.com/json/${userIp.ip}?fields=1622037`
  );
  const adress = await userAdress.json();
  const { continent, country, region, city, district } = adress;
  const ipInfo = new IpInfo(
    userIp.ip,
    continent,
    country,
    region,
    city,
    district
  );
  ipInfo.render();
}

class IpInfo {
  constructor(userIp, continent, country, region, city, district) {
    this.userIp = userIp;
    this.continent = continent;
    this.country = country;
    this.region = region;
    this.city = city;
    this.district = district;
  }
  render() {
    document.body.insertAdjacentHTML(
      "beforeend",
      `
        <div class= "js-div"></div>`
    );
    document.querySelector("div").insertAdjacentHTML("afterbegin", [
      `
        <h2>Your IP: ${this.userIp}</h2>
        <p>Your Contnent: ${this.continent}</p>
        <p>Your Country: ${this.country}</p>
        <p>Your Region: ${this.region}</p>
        <p>Your City: ${this.city}</p>
   `,
    ]);
  }
}
